import Vue from 'vue'
import Router from 'vue-router'
import signup from '../components/signup'
import chat from '../components/chat'

Vue.use(Router)

export default new Router({
  routes: [
    {
      path: '/chat',
      name: 'chat',
      component: chat
    },
    {
      path: '/',
      name: 'signup',
      component: signup
    }
  ],
  mode: 'history'
})
